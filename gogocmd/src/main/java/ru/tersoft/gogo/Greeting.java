package ru.tersoft.gogo;

/**
 * @author ivyanni
 * @version 1.0
 * OSGi service which prints greeting message.
 */
public interface Greeting {
    void hello(String name);
}
