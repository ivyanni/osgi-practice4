package ru.tersoft.gogo.impl;

import ru.tersoft.gogo.Greeting;

/**
 * @author ivyanni
 * @version 1.0
 * Implementation of service which prints greeting message for passed name.
 */
public class GreetingImpl implements Greeting {
    public void hello(String name) {
        System.out.println("Hello, " + name);
    }
}
