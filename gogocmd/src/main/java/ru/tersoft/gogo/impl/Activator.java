package ru.tersoft.gogo.impl;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import ru.tersoft.gogo.Greeting;

import java.util.Hashtable;

/**
 * @author ivyanni
 * @version 1.0
 * OSGi service activator.
 * Registers service and binds it to custom Gogo command.
 */
public class Activator implements BundleActivator {
    public void start(BundleContext bundleContext) throws Exception {
        Hashtable<String, String> props = new Hashtable<>();
        props.put("osgi.command.scope", "practice");
        props.put("osgi.command.function", "hello");
        bundleContext.registerService(Greeting.class.getName(), new GreetingImpl(), props);
    }

    public void stop(BundleContext bundleContext) throws Exception {

    }
}
